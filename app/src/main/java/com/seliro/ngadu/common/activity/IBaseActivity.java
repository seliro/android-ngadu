package com.seliro.ngadu.common.activity;

import com.seliro.ngadu.common.helper.PermissionManager;

/**
 * Created by Halim on 12/11/17.
 */

public interface IBaseActivity {

    PermissionManager getPermissionManager();
}
