package com.seliro.ngadu.common.viper;

import android.content.Context;
import android.util.Log;

import com.seliro.ngadu.modul.service.api.SeliroApi;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Halim on 15/12/17.
 */

public class BaseInteractor<T> implements BaseContract.Interactor<T> {

    private Context context;
    private BaseContract.InteractorOutput interactorOutput;

    public BaseInteractor(BaseContract.InteractorOutput interactorOutput) {
        this.interactorOutput = interactorOutput;
    }

    public BaseInteractor(Context context, BaseContract.InteractorOutput interactorOutput) {
        this.context = context;
        this.interactorOutput = interactorOutput;
    }

    @Override
    public void onLoad(SeliroApi seliroApi, Observable observable) {
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<T>() {
                    @Override
                    public void onCompleted() {
                        Log.d(getClass().getName(), "Kampret ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        interactorOutput.showMessageError(e.getMessage());
                    }

                    @Override
                    public void onNext(T wrapper) {
                        interactorOutput.onLoadSuccess(wrapper);
                    }
                });
    }

    @Override
    public void onButtonClick(SeliroApi seliroApi, Observable observable) {
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<T>() {
                    @Override
                    public void onCompleted() {
                        Log.d(getClass().getName(), "Kampret ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        interactorOutput.onButtonClickSuccess(context);
                    }

                    @Override
                    public void onNext(T wrapper) {
                        interactorOutput.onButtonClickSuccess(context);
                    }
                });
    }
}
