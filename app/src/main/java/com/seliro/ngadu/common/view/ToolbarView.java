package com.seliro.ngadu.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.seliro.ngadu.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 10/26/17.
 */

public class ToolbarView extends BaseView {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    public ToolbarView(Context context) {
        super(context);
        createView(context);
    }

    public ToolbarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ToolbarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    public ToolbarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.toolbar_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
    }

    public void setToolbarView(String title) {
        tvTitle.setText(title);
    }
}
