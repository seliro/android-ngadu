package com.seliro.ngadu.common.view;

import android.content.Context;
import android.view.View;

import com.seliro.ngadu.common.presenter.IBaseViewPresenter;


/**
 * Created by Halim on 10/26/17.
 */

public interface IBaseView {

    void createView(Context context);

    void bringToFrontOfBaseView(View view);

    void attachPresenter(IBaseViewPresenter iBaseViewPresenter);

    void checkConnection();

    void checkGpsLocation();
}
