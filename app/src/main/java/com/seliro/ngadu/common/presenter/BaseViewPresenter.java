package com.seliro.ngadu.common.presenter;

import com.seliro.ngadu.common.view.BaseView;

/**
 * Created by Halim on 10/26/17.
 */

public abstract class BaseViewPresenter implements IBaseViewPresenter {

    private BaseView baseView;

    @Override
    public void attachView(BaseView baseView) {
        this.baseView = baseView;
    }
}
