package com.seliro.ngadu.common.helper;

/**
 * Created by Halim on 10/26/17.
 */

public class SeliroConstans {

    public static class Preference {
        public static final String KEY = "com.seliro.ngadu.preference";
        public static final String KEY_DEVICE = "com.seliro.ngadu.preference.device";
    }

    public static class LoginSession {
        public static String GOOGLE_SIGNIN_NAME = "google-signin-name";
        public static String GOOGLE_EMAIL = "google-email";
        public static String GOOGLE_PHOTO_URL = "google-photo-url";
    }

    public static class Header {
        public static String DEVICE = "device";
        public static String LANG = "lang";
    }

    public static class Permission {
        public static int CAMERA_REQUEST_CODE = 711;
        public static int ALL_PERMISSION_REQUEST_CODE = 987;
        public static int GMAIL_REQUEST_CODE = 9187;
        public static int FACEBOOK_REQUEST_CODE = 9287;
        public static int PHONE_REQUEST_CODE = 9387;
        public static int LOCATION_REQUEST_CODE = 93872;
    }
}
