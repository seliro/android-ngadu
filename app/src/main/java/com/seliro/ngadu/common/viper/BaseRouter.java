package com.seliro.ngadu.common.viper;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Halim on 15/12/17.
 */

public class BaseRouter implements BaseContract.Router {

    private Context context;
    private Class clazz;

    public BaseRouter(Context context, Class clazz) {
        this.context = context;
        this.clazz = clazz;
    }

    @Override
    public void presentToNextScreen() {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }
}
