package com.seliro.ngadu.common.viper;

import android.content.Context;

import com.seliro.ngadu.modul.service.api.SeliroApi;

import rx.Observable;

/**
 * Created by Halim on 15/12/17.
 */

public class BasePresenter<T> implements BaseContract.Presenter<T>, BaseContract.InteractorOutput<T> {

    private Context context;
    private Class nextActivity;
    private BaseContract.View view;
    private BaseContract.Interactor interactor;
    private BaseContract.Router router;

    public BasePresenter(BaseContract.View view, Context context) {
        this.view = view;
        this.context = context;
        initPresenter();
    }

    public BasePresenter(BaseContract.View view, Context context, Class nextActivity) {
        this.view = view;
        this.context = context;
        this.nextActivity = nextActivity;
        initPresenterWithContext();
    }

    private void initPresenter() {
        interactor = new BaseInteractor(this);
    }

    private void initPresenterWithContext() {
        interactor = new BaseInteractor(context, this);
        router = new BaseRouter(context, nextActivity);
    }

    @Override
    public void onLoad(SeliroApi seliroApi, Observable observable) {
        interactor.onLoad(seliroApi, observable);
    }

    @Override
    public void onButtonClick(SeliroApi seliroApi, Observable observable) {
        interactor.onButtonClick(seliroApi, observable);
    }

    @Override
    public void onLoadSuccess(T wrapper) {
        view.setView(wrapper);
    }

    @Override
    public void onButtonClickSuccess(Context context) {
        router.presentToNextScreen();
    }

    @Override
    public void showMessageError(String message) {
        view.showMessageError(message);
    }
}
