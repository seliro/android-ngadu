package com.seliro.ngadu.common.application;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.seliro.ngadu.modul.component.ApplicationComponent;
import com.seliro.ngadu.modul.component.ApplicationModule;
import com.seliro.ngadu.modul.component.DaggerApplicationComponent;

/**
 * Created by Halim on 10/26/17.
 */

public class SeliroApplication extends MultiDexApplication {

    private ApplicationComponent applicationComponent;

    private static SeliroApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        applicationComponent = DaggerApplicationComponent.builder().
                applicationModule(new ApplicationModule(this)).
                build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public static SeliroApplication getInstance() {
        return instance;
    }
}
