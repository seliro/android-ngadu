package com.seliro.ngadu.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.seliro.ngadu.R;
import com.seliro.ngadu.common.helper.ConnectionDetector;
import com.seliro.ngadu.common.presenter.IBaseViewPresenter;

/**
 * Created by Halim on 10/26/17.
 */

public abstract class BaseView extends RelativeLayout implements IBaseView {

    NegativeScenarioView negativeScenarioView;

    private IBaseViewPresenter iBaseViewPresenter;

    public BaseView(Context context) {
        super(context);
    }

    public BaseView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public BaseView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void bringToFrontOfBaseView(View view) {
        getParent().bringChildToFront(view);
        getParent().requestLayout();
    }

    @Override
    public void createView(Context context) {
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        this.iBaseViewPresenter = iBaseViewPresenter;
    }

    @Override
    public void checkConnection() {
        addNegativeScenarioView();
        if (!ConnectionDetector.isNetworkConnected(getContext())) {
            negativeScenarioView.setNegativeScenarioView("No Connection", R.drawable.ic_close_popup);
            negativeScenarioView.setVisibility(GONE);
        } else {
            negativeScenarioView.setVisibility(VISIBLE);
        }
    }

    @Override
    public void checkGpsLocation() {
        addNegativeScenarioView();
    }

    private void addNegativeScenarioView() {
        if (negativeScenarioView == null) {
            negativeScenarioView = new NegativeScenarioView(getContext());
        }
    }
}
