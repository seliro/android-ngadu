package com.seliro.ngadu.common.presenter;

import com.seliro.ngadu.common.view.BaseView;

/**
 * Created by Halim on 10/26/17.
 */

public interface IBaseViewPresenter {

    void attachView(BaseView baseView);
}
