package com.seliro.ngadu.common.activity;

import android.support.v7.app.AppCompatActivity;

import com.seliro.ngadu.common.application.SeliroApplication;
import com.seliro.ngadu.modul.component.ApplicationComponent;

/**
 * Created by Halim on 10/26/17.
 */

public class BaseActivity extends AppCompatActivity {

    public BaseActivity() {
        prepareInjection();
    }

    protected void prepareInjection() {

    }

    protected ApplicationComponent getApplicationComponent() {
        ApplicationComponent applicationComponent = SeliroApplication.getInstance().getApplicationComponent();
        return SeliroApplication.getInstance().getApplicationComponent();
    }
}
