package com.seliro.ngadu.common.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.seliro.ngadu.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Halim on 12/11/17.
 */

public class NegativeScenarioView extends BaseView implements Animation.AnimationListener {

    @BindView(R.id.rl_negative_scenario)
    RelativeLayout rlNegativeScenario;

    @BindView(R.id.tv_message)
    TextView tvMessage;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    private static final int POPUP_TIMEOUT = 3500;
    private Handler handler = new Handler();
    private Runnable runnable = this::dismissNotif;

    public NegativeScenarioView(Context context) {
        super(context);
    }

    public NegativeScenarioView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NegativeScenarioView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NegativeScenarioView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.negative_scenario_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        rlNegativeScenario.setVisibility(View.GONE);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @OnClick(R.id.iv_icon)
    public void onIvIconClick() {
        dismissNotif();
    }

    public void setNegativeScenarioView(String message, int drawable) {
        tvMessage.setText(message);
        ivIcon.setImageResource(drawable);
        showNotification();
    }

    private void showNotification() {
        if (!rlNegativeScenario.isShown()) {
            rlNegativeScenario.setVisibility(View.VISIBLE);
            Animation slide_down = AnimationUtils.loadAnimation(getContext(),
                    R.anim.slide_in_down_from_top);
            slide_down.setInterpolator(new AccelerateDecelerateInterpolator());
            rlNegativeScenario.startAnimation(slide_down);
            handler.postDelayed(runnable, POPUP_TIMEOUT);
        }
    }

    private void dismissNotif() {
        if (rlNegativeScenario.isShown()) {
            Animation slide_up = AnimationUtils.loadAnimation(getContext(),
                    R.anim.slide_out_up_from_top);
            slide_up.setInterpolator(new AccelerateDecelerateInterpolator());
            slide_up.setAnimationListener(this);
            rlNegativeScenario.startAnimation(slide_up);
            handler.removeCallbacks(runnable);
        }
    }
}
