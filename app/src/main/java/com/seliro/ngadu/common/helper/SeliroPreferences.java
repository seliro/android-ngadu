package com.seliro.ngadu.common.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.seliro.ngadu.modul.service.request.LoginBody;

/**
 * Created by Halim on 23/11/17.
 */

public class SeliroPreferences {

    private SharedPreferences ngaduPreferences;

    public SeliroPreferences(Context context) {
        ngaduPreferences = context.getSharedPreferences(
                SeliroConstans.Preference.KEY, Context.MODE_PRIVATE);
    }

    public void setLoginSession(LoginBody loginBody) {
        SharedPreferences.Editor editor = ngaduPreferences.edit();
        editor.putString(SeliroConstans.LoginSession.GOOGLE_SIGNIN_NAME, loginBody.getGoogleSignInName());
        editor.putString(SeliroConstans.LoginSession.GOOGLE_EMAIL, loginBody.getGoogleEmail());
        editor.putString(SeliroConstans.LoginSession.GOOGLE_PHOTO_URL, loginBody.getGooglePhotoUrl().toString());
        editor.apply();
    }

    public LoginBody getLoginSession() {
        LoginBody loginBody = new LoginBody();
        loginBody.setGoogleSignInName(ngaduPreferences.getString(SeliroConstans.LoginSession.GOOGLE_SIGNIN_NAME, ""));
        loginBody.setGoogleEmail(ngaduPreferences.getString(SeliroConstans.LoginSession.GOOGLE_EMAIL, ""));
        loginBody.setGooglePhotoUrl(Uri.parse(ngaduPreferences.getString(SeliroConstans.LoginSession.GOOGLE_PHOTO_URL,"")));
        return loginBody;
    }
}
