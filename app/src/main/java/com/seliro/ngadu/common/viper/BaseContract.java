package com.seliro.ngadu.common.viper;

import android.content.Context;

import com.seliro.ngadu.modul.service.api.SeliroApi;

import rx.Observable;

/**
 * Created by Halim on 15/12/17.
 */

public class BaseContract {

    public interface View<T> {
        void setView(T wrapper);

        void showMessageError(String message);
    }

    interface Interactor<T> {
        void onLoad(SeliroApi seliroApi, Observable observable);

        void onButtonClick(SeliroApi seliroApi, Observable observable);
    }

    interface InteractorOutput<T> {

        void onLoadSuccess(T wrapper);

        void onButtonClickSuccess(Context context);

        void showMessageError(String message);

    }

    interface Presenter<T> {
        void onLoad(SeliroApi seliroApi, Observable observable);

        void onButtonClick(SeliroApi seliroApi, Observable observable);
    }

    interface Router {
        void presentToNextScreen();
    }
}
