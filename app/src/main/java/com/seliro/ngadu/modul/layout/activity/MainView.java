package com.seliro.ngadu.modul.layout.activity;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.seliro.ngadu.R;
import com.seliro.ngadu.common.adapter.ViewPagerAdapter;
import com.seliro.ngadu.common.view.BaseView;
import com.seliro.ngadu.modul.layout.fragment.InviteFragment;
import com.seliro.ngadu.modul.layout.fragment.ProfileFragment;
import com.seliro.ngadu.modul.layout.fragment.RewardFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 12/11/17.
 */

public class MainView extends BaseView {

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    private AppCompatActivity activity;

    public MainView(Context context) {
        super(context);
        createView(context);
    }

    public MainView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public MainView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    public MainView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.main_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = activity;
        initView();
        setupTabIcons();
    }


    private void initView() {
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.home,
                R.drawable.reward,
                R.drawable.profile
        };

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(activity.getSupportFragmentManager());
        viewPagerAdapter.addFrag(new InviteFragment(), "Home");
        viewPagerAdapter.addFrag(new RewardFragment(), "Reward");
        viewPagerAdapter.addFrag(new ProfileFragment(), "Profile");
        viewPager.setAdapter(viewPagerAdapter);
    }

}
