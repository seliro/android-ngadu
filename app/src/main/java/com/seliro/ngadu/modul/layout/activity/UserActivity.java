package com.seliro.ngadu.modul.layout.activity;

import android.content.Context;
import android.os.Bundle;

import com.seliro.ngadu.R;
import com.seliro.ngadu.common.activity.BaseActivity;
import com.seliro.ngadu.common.view.ToolbarView;
import com.seliro.ngadu.modul.component.ActivityComponent;
import com.seliro.ngadu.modul.component.DaggerActivityComponent;
import com.seliro.ngadu.modul.layout.view.UserView;
import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.request.UserBody;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserActivity extends BaseActivity {

    @Inject
    Context context;

    @Inject
    SeliroApi seliroApi;

    @BindView(R.id.seliro_view)
    UserView userView;

    @BindView(R.id.toolbar_view)
    ToolbarView toolbarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {
        toolbarView.setToolbarView("Profile");
        UserBody userBody = new UserBody();
        userBody.setId(2);
        userView.getSeliroPresenter().loadSeliroData(seliroApi, userBody);
    }

}
