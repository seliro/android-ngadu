package com.seliro.ngadu.modul.layout.view;

import com.seliro.ngadu.common.view.IBaseView;
import com.seliro.ngadu.modul.model.Invite;
import com.seliro.ngadu.modul.presenter.IinvitePresenter;

import java.util.List;

/**
 * Created by Halim on 26/11/17.
 */

public interface IinviteView extends IBaseView {

    void setInviteView(List<Invite> invites);

    IinvitePresenter getInvitePresenter();
}
