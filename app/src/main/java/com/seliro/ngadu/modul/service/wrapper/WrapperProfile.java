package com.seliro.ngadu.modul.service.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seliro.ngadu.modul.model.Profile;
import com.seliro.ngadu.modul.model.ResponseData;

/**
 * Created by Halim on 12/11/17.
 */

public class WrapperProfile extends ResponseData {

    @SerializedName("data")
    @Expose
    private Profile profile;

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
