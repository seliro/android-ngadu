package com.seliro.ngadu.modul.layout.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seliro.ngadu.R;
import com.seliro.ngadu.modul.layout.viewholder.ProfileStatusViewHolder;
import com.seliro.ngadu.modul.model.ProfileStatus;

import java.util.List;

/**
 * Created by Halim on 18/11/17.
 */

public class ProfileStatusAdapter extends RecyclerView.Adapter<ProfileStatusViewHolder> {

    private Context context;
    private List<ProfileStatus> profileStatuses;

    public ProfileStatusAdapter(Context context, List<ProfileStatus> profileStatuses) {
        this.context = context;
        this.profileStatuses = profileStatuses;
    }

    @Override
    public ProfileStatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_status, parent, false);
        return new ProfileStatusViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProfileStatusViewHolder holder, int position) {
        ProfileStatus profileStatus = profileStatuses.get(position);
        holder.setProfileStatusView(context, profileStatus);
    }

    @Override
    public int getItemCount() {
        return profileStatuses != null ? profileStatuses.size() : 0;
    }

    public void addProfileStatuses(List<ProfileStatus> profileStatuses) {
        this.profileStatuses = profileStatuses;
        notifyDataSetChanged();
    }
}
