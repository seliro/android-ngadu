package com.seliro.ngadu.modul.layout.view;


import com.seliro.ngadu.common.view.IBaseView;
import com.seliro.ngadu.modul.model.Profile;
import com.seliro.ngadu.modul.presenter.IProfilePresenter;

/**
 * Created by Halim on 12/11/17.
 */

public interface IProfileView extends IBaseView {

    void setProfileView(Profile profile);

    IProfilePresenter getProfilePresenter();
}
