package com.seliro.ngadu.modul.login;

import android.content.Context;

import com.seliro.ngadu.common.viper.BaseContract;
import com.seliro.ngadu.common.viper.BasePresenter;
import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.wrapper.WrapperLogin;

import rx.Observable;

/**
 * Created by Halim on 15/12/17.
 */

public class LoginPresenter extends BasePresenter<WrapperLogin> {

    public LoginPresenter(BaseContract.View view, Context context, Class nextActivity) {
        super(view, context, nextActivity);
    }

    @Override
    public void onButtonClick(SeliroApi seliroApi, Observable observable) {
        super.onButtonClick(seliroApi, observable);
    }

    @Override
    public void onButtonClickSuccess(Context context) {
        super.onButtonClickSuccess(context);
    }

    @Override
    public void showMessageError(String message) {
        super.showMessageError(message);
    }
}
