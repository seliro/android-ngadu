package com.seliro.ngadu.modul.presenter;

import com.seliro.ngadu.common.presenter.IBaseViewPresenter;
import com.seliro.ngadu.modul.service.api.SeliroApi;

/**
 * Created by Halim on 23/11/17.
 */

public interface IInterestPresenter extends IBaseViewPresenter {

    void loadInterestData(SeliroApi seliroApi);
}
