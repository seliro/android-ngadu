package com.seliro.ngadu.modul.layout.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.seliro.ngadu.R;
import com.seliro.ngadu.common.presenter.IBaseViewPresenter;
import com.seliro.ngadu.common.view.BaseView;
import com.seliro.ngadu.modul.model.User;
import com.seliro.ngadu.modul.presenter.IUserPresenter;
import com.seliro.ngadu.modul.presenter.UserPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 10/26/17.
 */

public class UserView extends BaseView implements IUserView {

    private IUserPresenter iUserPresenter;

    @BindView(R.id.tv_username)
    TextView tvUserName;

    @BindView(R.id.tv_email)
    TextView tvEmail;

    public UserView(Context context) {
        super(context);
        createView(context);
    }

    public UserView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public UserView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    public UserView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.user_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new UserPresenter());
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iUserPresenter = (IUserPresenter) iBaseViewPresenter;
        iUserPresenter.attachView(this);
    }

    @Override
    public void setSeliroView(User user) {
        tvUserName.setText(user.getUserName());
        tvEmail.setText(user.getEmail());
    }

    @Override
    public void noDataFound(String message) {

    }

    @Override
    public IUserPresenter getSeliroPresenter() {
        return iUserPresenter;
    }
}
