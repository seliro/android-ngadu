package com.seliro.ngadu.modul.interest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.seliro.ngadu.R;
import com.seliro.ngadu.common.presenter.IBaseViewPresenter;
import com.seliro.ngadu.common.view.BaseView;
import com.seliro.ngadu.common.view.FixedGridLayoutManager;
import com.seliro.ngadu.common.view.ItemOffsetDecoration;
import com.seliro.ngadu.modul.layout.activity.MainActivity;
import com.seliro.ngadu.modul.layout.adapter.InterestAdapter;
import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.wrapper.WrapperInterest;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Halim on 16/12/17.
 */

public class InterestView extends BaseView implements InterestContract.View<WrapperInterest> {

    @BindView(R.id.rv_interest)
    RecyclerView rvInterest;

    @BindView(R.id.b_Choose)
    Button bChoose;

    private InterestAdapter interestAdapter;

    public InterestView(Context context) {
        super(context);
        createView(context);
    }

    public InterestView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public InterestView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    public InterestView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.interest_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        initView();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
    }

    private void initView() {
        FixedGridLayoutManager gridLayoutManager = new FixedGridLayoutManager(getContext(), 3);
        interestAdapter = new InterestAdapter(getContext(), new ArrayList<>());
        rvInterest.setLayoutManager(gridLayoutManager);
        rvInterest.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.five_dp));
        rvInterest.setAdapter(interestAdapter);
    }

    public void loadPresenter(SeliroApi seliroApi) {
        InterestPresenter interestPresenter = new InterestPresenter(this, getContext(), MainActivity.class);
        interestPresenter.onLoad(seliroApi, seliroApi.getInterest());
    }

    @OnClick(R.id.b_Choose)
    public void onButtonChooseClick() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        getContext().startActivity(intent);
    }

    @Override
    public void setView(WrapperInterest wrapperInterest) {
        interestAdapter.addInterests(wrapperInterest.getInterests());
    }

    @Override
    public void showMessageError(String message) {

    }
}
