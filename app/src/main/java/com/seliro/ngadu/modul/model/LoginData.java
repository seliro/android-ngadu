package com.seliro.ngadu.modul.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 26/11/17.
 */

public class LoginData {

    @SerializedName("login_token")
    @Expose
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
