package com.seliro.ngadu.modul.component;


import com.seliro.ngadu.modul.interest.InterestActivity;
import com.seliro.ngadu.modul.layout.activity.MainActivity;
import com.seliro.ngadu.modul.layout.activity.UserActivity;

import dagger.Component;

/**
 * Created by Halim on 10/26/17.
 */

@PerActivity
@Component(modules = ActivityModule.class, dependencies = ApplicationComponent.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(UserActivity userActivity);

    void inject(InterestActivity interestActivity);
}
