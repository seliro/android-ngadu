package com.seliro.ngadu.modul.service.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seliro.ngadu.modul.model.LoginData;
import com.seliro.ngadu.modul.model.ResponseData;

/**
 * Created by Halim on 26/11/17.
 */

public class WrapperLogin extends ResponseData {

    @SerializedName("data")
    @Expose
    private LoginData loginData;

    public LoginData getLoginData() {
        return loginData;
    }

    public void setLoginData(LoginData loginData) {
        this.loginData = loginData;
    }
}
