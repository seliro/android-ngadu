package com.seliro.ngadu.modul.layout.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seliro.ngadu.R;
import com.seliro.ngadu.common.fragment.BaseFragment;
import com.seliro.ngadu.modul.layout.view.InviteView;
import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.api.SeliroApiGenerator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 12/11/17.
 */

public class InviteFragment extends BaseFragment {

    @BindView(R.id.invite_view)
    InviteView inviteView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_home,container,false);
        ButterKnife.bind(this,view);
        initView();
        return view;
    }

    private void initView() {
        SeliroApi seliroApi = SeliroApiGenerator.createService(getContext());
        inviteView.getInvitePresenter().loadInviteData(seliroApi);
    }
}
