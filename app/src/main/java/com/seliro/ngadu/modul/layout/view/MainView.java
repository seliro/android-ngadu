package com.seliro.ngadu.modul.layout.view;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;

import com.seliro.ngadu.common.view.BaseView;
import com.seliro.ngadu.modul.layout.fragment.InviteFragment;
import com.seliro.ngadu.modul.layout.fragment.ProfileFragment;
import com.seliro.ngadu.modul.layout.fragment.RewardFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Halim on 12/11/17.
 */

public class MainView extends BaseView implements IMainView {

    private InviteFragment inviteFragment;
    private RewardFragment rewardFragment;
    private ProfileFragment profileFragment;

    public MainView(Context context) {
        super(context);
    }

    public MainView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MainView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MainView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private List<Fragment> getFragments() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(inviteFragment);
        fragments.add(rewardFragment);
        fragments.add(profileFragment);
        return fragments;
    }

}
