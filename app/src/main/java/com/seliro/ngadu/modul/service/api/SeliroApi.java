package com.seliro.ngadu.modul.service.api;

import com.seliro.ngadu.modul.service.request.InviteBody;
import com.seliro.ngadu.modul.service.request.LoginBody;
import com.seliro.ngadu.modul.service.wrapper.WrapperInterest;
import com.seliro.ngadu.modul.service.wrapper.WrapperInvite;
import com.seliro.ngadu.modul.service.wrapper.WrapperLogin;
import com.seliro.ngadu.modul.service.wrapper.WrapperProfile;
import com.seliro.ngadu.modul.service.wrapper.WrapperUser;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Halim on 10/26/17.
 */

public interface SeliroApi {

    @POST("api/v1/static_user")
    Observable<WrapperLogin> postLoginData(
            @Body LoginBody loginBody);


    @GET("api/v1/interests")
    Observable<WrapperInterest> getInterest();

    @POST("api/v1/invite_athlete")
    Observable<WrapperInvite> postInviteAthlete(
            @Body InviteBody inviteBody);

    @GET("api/v1/static_user")
    Observable<WrapperUser> getUser(
            @Query("data") String data);

    @GET("api/v1/profile")
    Observable<WrapperProfile> getProfile(
            @Query("data") String data);
}
