package com.seliro.ngadu.modul.presenter;


import com.seliro.ngadu.common.presenter.IBaseViewPresenter;
import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.request.UserBody;

/**
 * Created by Halim on 10/26/17.
 */

public interface IUserPresenter extends IBaseViewPresenter {

    void loadSeliroData(SeliroApi seliroApi, UserBody userBody);
}
