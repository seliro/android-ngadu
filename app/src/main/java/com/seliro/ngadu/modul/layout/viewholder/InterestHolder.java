package com.seliro.ngadu.modul.layout.viewholder;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.seliro.ngadu.R;
import com.seliro.ngadu.modul.model.Interest;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 23/11/17.
 */

public class InterestHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.rl_interest)
    RelativeLayout rlInterest;

    @BindView(R.id.iv_interest)
    ImageView ivInterest;

    @BindView(R.id.tv_interest)
    TextView tvInterest;

    public InterestHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setInterestHolderView(Context context, Interest interest) {
        Glide.with(context).load(interest.getUrlLogo()).into(ivInterest);
        tvInterest.setText(interest.getInterestName());
    }
}
