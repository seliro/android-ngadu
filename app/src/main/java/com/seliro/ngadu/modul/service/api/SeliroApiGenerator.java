package com.seliro.ngadu.modul.service.api;

import android.content.Context;

import com.seliro.ngadu.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Halim on 10/26/17.
 */

public class SeliroApiGenerator {

    public static <S> SeliroApi createService(Context context) {
        return retroBuilder(okBuilder(context).build()).create(SeliroApi.class);
    }

    private static Retrofit retroBuilder(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.SELIRO_NGADU_API)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private static OkHttpClient.Builder okBuilder(Context context) {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(chain -> chain.proceed(requestBuilder(chain, context)));
    }

    private static Request requestBuilder(Interceptor.Chain chain, Context context) {
        Request.Builder requestBuilder = chain.request().newBuilder();
        RequestBuilderHeader requestBuilderHeader = new RequestBuilderHeader(context, requestBuilder);
        requestBuilderHeader.addDevice();
        requestBuilderHeader.addLang();
        return requestBuilder.build();
    }
}
