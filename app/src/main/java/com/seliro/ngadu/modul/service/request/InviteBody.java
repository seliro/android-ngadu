package com.seliro.ngadu.modul.service.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Halim on 26/11/17.
 */

public class InviteBody {

    @SerializedName("ids")
    @Expose
    private List<Integer> id;

    public List<Integer> getId() {
        return id;
    }

    public void setId(List<Integer> id) {
        this.id = id;
    }
}
