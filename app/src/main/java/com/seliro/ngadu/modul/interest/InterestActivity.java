package com.seliro.ngadu.modul.interest;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.seliro.ngadu.R;
import com.seliro.ngadu.common.activity.BaseActivity;
import com.seliro.ngadu.modul.component.ActivityComponent;
import com.seliro.ngadu.modul.component.DaggerActivityComponent;
import com.seliro.ngadu.modul.service.api.SeliroApi;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 16/12/17.
 */

public class InterestActivity extends BaseActivity {

    @Inject
    Context context;

    @Inject
    SeliroApi seliroApi;

    @BindView(R.id.interest_view)
    InterestView interestView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void init() {
        interestView.loadPresenter(seliroApi);
    }
}
