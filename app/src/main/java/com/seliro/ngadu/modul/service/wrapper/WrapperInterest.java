package com.seliro.ngadu.modul.service.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seliro.ngadu.modul.model.Interest;
import com.seliro.ngadu.modul.model.ResponseData;

import java.util.List;

/**
 * Created by Halim on 26/11/17.
 */

public class WrapperInterest extends ResponseData {

    @SerializedName("data")
    @Expose
    private List<Interest> interests;

    public List<Interest> getInterests() {
        return interests;
    }

    public void setInterests(List<Interest> interests) {
        this.interests = interests;
    }
}
