package com.seliro.ngadu.modul.layout.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.seliro.ngadu.R;
import com.seliro.ngadu.common.activity.BaseActivity;
import com.seliro.ngadu.common.helper.SeliroPreferences;
import com.seliro.ngadu.modul.component.ActivityComponent;
import com.seliro.ngadu.modul.component.DaggerActivityComponent;
import com.seliro.ngadu.modul.interest.InterestActivity;
import com.seliro.ngadu.modul.service.request.LoginBody;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.main_view)
    MainView mainView;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private ImageView ivUserPhoto;
    private TextView tvUserName;
    private TextView tvUserEmail;

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initNavigationView();
        initView();
        initListener();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
        } else if (id == R.id.nav_edit_interest) {
            Intent intent = new Intent(this, InterestActivity.class);
            this.startActivity(intent);
        } else if (id == R.id.nav_chalenge_history) {

        } else if (id == R.id.nav_reward_history) {

        } else if (id == R.id.nav_invitation_history) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_vote) {

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    private void initNavigationView() {
        View hView = navigationView.getHeaderView(0);
        ivUserPhoto = hView.findViewById(R.id.iv_user_photo);
        tvUserName = hView.findViewById(R.id.tv_user_name);
        tvUserEmail = hView.findViewById(R.id.tv_user_email);

        LoginBody loginBody = new SeliroPreferences(this).getLoginSession();
        Glide.with(this).load(loginBody.getGooglePhotoUrl()).into(ivUserPhoto);
        tvUserName.setText(loginBody.getGoogleSignInName());
        tvUserEmail.setText(loginBody.getGoogleEmail());
    }

    private void initView() {
        mainView.setActivity(this);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initListener() {
        fab.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(this);
    }
}
