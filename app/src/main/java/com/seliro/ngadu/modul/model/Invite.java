package com.seliro.ngadu.modul.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 26/11/17.
 */

public class Invite {

    @SerializedName("full_name")
    @Expose
    private String fullName;

    @SerializedName("interest_image_url")
    @Expose
    private String interestImageUrl;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("level")
    @Expose
    private int level;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getInterestImageUrl() {
        return interestImageUrl;
    }

    public void setInterestImageUrl(String interestImageUrl) {
        this.interestImageUrl = interestImageUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
