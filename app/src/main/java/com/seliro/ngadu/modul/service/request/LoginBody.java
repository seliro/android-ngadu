package com.seliro.ngadu.modul.service.request;

import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 23/11/17.
 */

public class LoginBody {

    @SerializedName("google_id")
    @Expose
    private String googleId;

    @SerializedName("full_name")
    @Expose
    private String googleSignInName;

    @SerializedName("email")
    @Expose
    private String googleEmail;

    @SerializedName("avatar_url")
    @Expose
    private Uri googlePhotoUrl;

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getGoogleSignInName() {
        return googleSignInName;
    }

    public void setGoogleSignInName(String googleSignInName) {
        this.googleSignInName = googleSignInName;
    }

    public String getGoogleEmail() {
        return googleEmail;
    }

    public void setGoogleEmail(String googleEmail) {
        this.googleEmail = googleEmail;
    }

    public Uri getGooglePhotoUrl() {
        return googlePhotoUrl;
    }

    public void setGooglePhotoUrl(Uri googlePhotoUrl) {
        this.googlePhotoUrl = googlePhotoUrl;
    }
}
