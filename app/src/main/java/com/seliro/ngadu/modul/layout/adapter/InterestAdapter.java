package com.seliro.ngadu.modul.layout.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seliro.ngadu.R;
import com.seliro.ngadu.modul.layout.viewholder.InterestHolder;
import com.seliro.ngadu.modul.model.Interest;

import java.util.List;

/**
 * Created by Halim on 23/11/17.
 */

public class InterestAdapter extends RecyclerView.Adapter<InterestHolder> {

    private Context context;
    private List<Interest> interests;

    public InterestAdapter(Context context, List<Interest> interests) {
        this.context = context;
        this.interests = interests;
    }

    @Override
    public InterestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_interest, parent, false);
        return new InterestHolder(view);
    }

    @Override
    public void onBindViewHolder(InterestHolder holder, int position) {
        Interest interest = interests.get(position);
        holder.setInterestHolderView(context, interest);
    }

    @Override
    public int getItemCount() {
        return interests.size();
    }

    public void addInterests(List<Interest> interests) {
        this.interests = interests;
        notifyDataSetChanged();
    }
}
