package com.seliro.ngadu.modul.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 18/11/17.
 */

public class ProfileStatus implements Parcelable {

    @SerializedName("logo")
    @Expose
    private String logo;

    @SerializedName("count")
    @Expose
    private int count;

    @SerializedName("name")
    @Expose
    private String name;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.logo);
        dest.writeInt(this.count);
        dest.writeString(this.name);
    }

    public ProfileStatus() {
    }

    protected ProfileStatus(Parcel in) {
        this.logo = in.readString();
        this.count = in.readInt();
        this.name = in.readString();
    }

    public static final Creator<ProfileStatus> CREATOR = new Creator<ProfileStatus>() {
        @Override
        public ProfileStatus createFromParcel(Parcel source) {
            return new ProfileStatus(source);
        }

        @Override
        public ProfileStatus[] newArray(int size) {
            return new ProfileStatus[size];
        }
    };
}
