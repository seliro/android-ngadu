package com.seliro.ngadu.modul.layout.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seliro.ngadu.R;
import com.seliro.ngadu.common.fragment.BaseFragment;

/**
 * Created by Halim on 12/11/17.
 */

public class RewardFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_reward,container,false);
        return view;
    }
}
