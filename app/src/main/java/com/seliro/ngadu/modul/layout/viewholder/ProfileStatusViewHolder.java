package com.seliro.ngadu.modul.layout.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.seliro.ngadu.R;
import com.seliro.ngadu.modul.model.ProfileStatus;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 18/11/17.
 */

public class ProfileStatusViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_logo)
    ImageView ivLogo;

    @BindView(R.id.tv_status_count)
    TextView tvStatusCount;

    @BindView(R.id.tv_status_name)
    TextView tvStatusName;

    public ProfileStatusViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setProfileStatusView(Context context, ProfileStatus profileStatus) {
        Glide.with(context).load(profileStatus.getLogo());
        tvStatusCount.setText(profileStatus.getCount());
        tvStatusName.setText(profileStatus.getName());
    }
}
