package com.seliro.ngadu.modul.component;

import android.content.Context;

import com.seliro.ngadu.modul.service.api.SeliroApi;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Halim on 10/26/17.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    Context exposeContext();

    SeliroApi exposeSeliroApi();
}
