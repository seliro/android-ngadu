package com.seliro.ngadu.modul.component;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Halim on 10/26/17.
 */

@Module
public class ActivityModule {

    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    Activity provideActivity() {
        return activity;
    }
}
