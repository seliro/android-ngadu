package com.seliro.ngadu.modul.interest;

import android.content.Context;

import com.seliro.ngadu.common.viper.BaseContract;
import com.seliro.ngadu.common.viper.BasePresenter;
import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.wrapper.WrapperInterest;

import rx.Observable;

/**
 * Created by Halim on 15/12/17.
 */

public class InterestPresenter extends BasePresenter<WrapperInterest> {

    public InterestPresenter(BaseContract.View view, Context context, Class nextActivity) {
        super(view, context, nextActivity);
    }

    @Override
    public void onLoad(SeliroApi seliroApi, Observable observable) {
        super.onLoad(seliroApi, observable);
    }

    @Override
    public void onLoadSuccess(WrapperInterest wrapper) {
        super.onLoadSuccess(wrapper);
    }

    @Override
    public void showMessageError(String message) {
        super.showMessageError(message);
    }
}
