package com.seliro.ngadu.modul.layout.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seliro.ngadu.R;
import com.seliro.ngadu.common.fragment.BaseFragment;
import com.seliro.ngadu.modul.layout.view.ProfileView;
import com.seliro.ngadu.modul.service.api.SeliroApiGenerator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 12/11/17.
 */

public class ProfileFragment extends BaseFragment {

    @BindView(R.id.profile_view)
    ProfileView profileView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        profileView.getProfilePresenter().loadProfileData(SeliroApiGenerator.createService(getContext()));
    }
}
