package com.seliro.ngadu.modul.presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.seliro.ngadu.common.presenter.BaseViewPresenter;
import com.seliro.ngadu.common.view.BaseView;
import com.seliro.ngadu.modul.layout.view.ProfileView;
import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.request.UserBody;
import com.seliro.ngadu.modul.service.wrapper.WrapperProfile;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Halim on 12/11/17.
 */

public class ProfilePresenter extends BaseViewPresenter implements IProfilePresenter {

    private ProfileView profileView;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        profileView = (ProfileView) baseView;
    }

    @Override
    public void loadProfileData(SeliroApi seliroApi) {
        UserBody userBody = new UserBody();
        userBody.setId(1);
        seliroApi.getProfile(new Gson().toJson(userBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Subscriber<WrapperProfile>() {

                            @Override
                            public void onCompleted() {
                                Log.d(getClass().getName(), "Kampret ");
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(getClass().getName(), "ERROR " + e);

                            }

                            @Override
                            public void onNext(WrapperProfile wrapperProfile) {
                                Log.d(getClass().getName(), "Kampret " + wrapperProfile.getProfile());
                                profileView.setProfileView(wrapperProfile.getProfile());
                            }
                        });
    }
}
