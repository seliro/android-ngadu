package com.seliro.ngadu.modul.component;

import android.content.Context;

import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.api.SeliroApiGenerator;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Halim on 10/26/17.
 */

@Module
public class ApplicationModule {

    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext() {
        return context;
    }

    @Provides
    public SeliroApi provideSeliroApi() {
        return SeliroApiGenerator.createService(context);
    }
}
