package com.seliro.ngadu.modul.service.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seliro.ngadu.modul.model.ResponseData;
import com.seliro.ngadu.modul.model.User;

/**
 * Created by Halim on 10/26/17.
 */

public class WrapperUser extends ResponseData {

    @SerializedName("data")
    @Expose
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
