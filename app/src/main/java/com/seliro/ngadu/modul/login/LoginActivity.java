package com.seliro.ngadu.modul.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.Task;
import com.seliro.ngadu.R;
import com.seliro.ngadu.common.activity.BaseActivity;
import com.seliro.ngadu.common.helper.SeliroPreferences;
import com.seliro.ngadu.modul.interest.InterestActivity;
import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.api.SeliroApiGenerator;
import com.seliro.ngadu.modul.service.request.LoginBody;
import com.seliro.ngadu.modul.service.wrapper.WrapperLogin;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Halim on 15/12/17.
 */

public class LoginActivity extends BaseActivity implements LoginContract.View<WrapperLogin> {

    @BindView(R.id.sign_in_button)
    SignInButton signInButton;

    GoogleSignInClient mGoogleSignInClient;

    private LoginPresenter loginPresenter;

    public static int RC_SIGN_IN = 100;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loginPresenter = new LoginPresenter(this, this, InterestActivity.class);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

    }

    @OnClick(R.id.sign_in_button)
    public void onSignInButtonClick() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            LoginBody loginBody = new LoginBody();
            loginBody.setGoogleId(acct.getId());
            loginBody.setGoogleSignInName(acct.getDisplayName());
            loginBody.setGoogleEmail(acct.getEmail());
            loginBody.setGooglePhotoUrl(acct.getPhotoUrl());
            SeliroPreferences seliroPreferences = new SeliroPreferences(this);
            seliroPreferences.setLoginSession(loginBody);
            SeliroApi seliroApi = SeliroApiGenerator.createService(this);
            loginPresenter.onButtonClick(seliroApi, seliroApi.postLoginData(loginBody));
        }

    }

    @Override
    public void setView(WrapperLogin wrapper) {

    }

    @Override
    public void showMessageError(String message) {
        Log.d(getClass().getName(), "Error " + message);
    }
}
