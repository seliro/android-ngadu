package com.seliro.ngadu.modul.layout.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.seliro.ngadu.R;
import com.seliro.ngadu.common.presenter.IBaseViewPresenter;
import com.seliro.ngadu.common.view.BaseView;
import com.seliro.ngadu.modul.layout.adapter.ProfileStatusAdapter;
import com.seliro.ngadu.modul.model.Profile;
import com.seliro.ngadu.modul.presenter.IProfilePresenter;
import com.seliro.ngadu.modul.presenter.ProfilePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Halim on 12/11/17.
 */

public class ProfileView extends BaseView implements IProfileView {

    private ProfileStatusAdapter profileStatusAdapter;
    private IProfilePresenter iProfilePresenter;

    @BindView(R.id.iv_interest)
    ImageView ivInterest;

    @BindView(R.id.iv_user_photo)
    ImageView ivUserPhoto;

    @BindView(R.id.tv_user_name)
    TextView tvUserName;

    @BindView(R.id.tv_level)
    TextView tvLevel;

    @BindView(R.id.tv_point)
    TextView tvPoint;

    public ProfileView(Context context) {
        super(context);
        createView(context);
    }

    public ProfileView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ProfileView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    public ProfileView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.profile_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new ProfilePresenter());
        initView();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iProfilePresenter = (IProfilePresenter) iBaseViewPresenter;
        iProfilePresenter.attachView(this);
    }

    @Override
    public void setProfileView(Profile profile) {

    }

    @Override
    public IProfilePresenter getProfilePresenter() {
        return iProfilePresenter;
    }

    @OnClick(R.id.tv_user_detail)
    public void onTvUserDetailClick() {

    }

    private void initView() {

    }
}
