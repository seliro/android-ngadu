package com.seliro.ngadu.modul.service.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seliro.ngadu.modul.model.Invite;
import com.seliro.ngadu.modul.model.ResponseData;

import java.util.List;

/**
 * Created by Halim on 26/11/17.
 */

public class WrapperInvite extends ResponseData {

    @SerializedName("data")
    @Expose
    private List<Invite> invites;

    public List<Invite> getInvites() {
        return invites;
    }

    public void setInvites(List<Invite> invites) {
        this.invites = invites;
    }
}
