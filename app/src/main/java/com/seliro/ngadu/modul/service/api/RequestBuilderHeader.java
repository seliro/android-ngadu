package com.seliro.ngadu.modul.service.api;

import android.content.Context;

import com.seliro.ngadu.common.helper.DeviceUtil;
import com.seliro.ngadu.common.helper.SeliroConstans;

import java.util.Locale;

import okhttp3.Request;

/**
 * Created by Halim on 10/26/17.
 */

public class RequestBuilderHeader {

    private Context context;
    private Request.Builder requestBuilder;

    public RequestBuilderHeader(Context context, Request.Builder requestBuilder) {
        this.context = context;
        this.requestBuilder = requestBuilder;
    }

    public void addDevice() {
        requestBuilder.addHeader(SeliroConstans.Header.DEVICE, DeviceUtil.getDeviceID(context));
    }

    public void addLang() {
        requestBuilder.addHeader(SeliroConstans.Header.LANG, Locale.getDefault().toString());
    }
}
