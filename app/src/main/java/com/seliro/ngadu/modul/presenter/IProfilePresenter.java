package com.seliro.ngadu.modul.presenter;

import com.seliro.ngadu.common.presenter.IBaseViewPresenter;
import com.seliro.ngadu.modul.service.api.SeliroApi;

/**
 * Created by Halim on 12/11/17.
 */

public interface IProfilePresenter extends IBaseViewPresenter {

    void loadProfileData(SeliroApi seliroApi);
}
