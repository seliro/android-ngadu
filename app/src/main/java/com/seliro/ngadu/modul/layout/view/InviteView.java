package com.seliro.ngadu.modul.layout.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.seliro.ngadu.R;
import com.seliro.ngadu.common.presenter.IBaseViewPresenter;
import com.seliro.ngadu.common.view.BaseView;
import com.seliro.ngadu.modul.model.Invite;
import com.seliro.ngadu.modul.presenter.IinvitePresenter;
import com.seliro.ngadu.modul.presenter.InvitePresenter;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Halim on 26/11/17.
 */

public class InviteView extends BaseView implements IinviteView {

    private IinvitePresenter iinvitePresenter;

    public InviteView(Context context) {
        super(context);
        createView(context);
    }

    public InviteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public InviteView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    public InviteView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.invite_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new InvitePresenter());
        initView();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iinvitePresenter = (IinvitePresenter) iBaseViewPresenter;
        iinvitePresenter.attachView(this);
    }

    private void initView() {

    }

    @Override
    public void setInviteView(List<Invite> invites) {

    }

    @Override
    public IinvitePresenter getInvitePresenter() {
        return iinvitePresenter;
    }
}
