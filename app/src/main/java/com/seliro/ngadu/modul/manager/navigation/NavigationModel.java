package com.seliro.ngadu.modul.manager.navigation;

/**
 * Created by Halim on 21/11/17.
 */

public interface NavigationModel {

    int getNavigationId();

    void startNavigation();


}
