package com.seliro.ngadu.modul.layout.view;


import com.seliro.ngadu.common.view.IBaseView;
import com.seliro.ngadu.modul.model.User;
import com.seliro.ngadu.modul.presenter.IUserPresenter;

/**
 * Created by Halim on 10/26/17.
 */

public interface IUserView extends IBaseView {

    void setSeliroView(User user);

    void noDataFound(String message);

    IUserPresenter getSeliroPresenter();
}
