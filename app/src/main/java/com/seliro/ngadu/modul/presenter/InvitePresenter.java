package com.seliro.ngadu.modul.presenter;

import android.util.Log;

import com.seliro.ngadu.common.presenter.BaseViewPresenter;
import com.seliro.ngadu.common.view.BaseView;
import com.seliro.ngadu.modul.layout.view.IinviteView;
import com.seliro.ngadu.modul.layout.view.InviteView;
import com.seliro.ngadu.modul.model.Invite;
import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.request.InviteBody;
import com.seliro.ngadu.modul.service.wrapper.WrapperInvite;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Halim on 26/11/17.
 */

public class InvitePresenter extends BaseViewPresenter implements IinvitePresenter {

    private InviteView inviteView;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        inviteView = (InviteView) baseView;
    }

    @Override
    public void loadInviteData(SeliroApi seliroApi) {
        InviteBody inviteBody = new InviteBody();
        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        inviteBody.setId(ids);
        seliroApi.postInviteAthlete(inviteBody).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Subscriber<WrapperInvite>() {

                            @Override
                            public void onCompleted() {
                                Log.d(getClass().getName(), "Kampret ");
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(getClass().getName(), "ERROR " + e);

                            }

                            @Override
                            public void onNext(WrapperInvite wrapperInvite) {
                                Log.d(getClass().getName(), "Kampret " + wrapperInvite.getInvites());
                                inviteView.setInviteView(wrapperInvite.getInvites());
                            }
                        });
    }
}

