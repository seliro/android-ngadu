package com.seliro.ngadu.modul.presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.seliro.ngadu.common.presenter.BaseViewPresenter;
import com.seliro.ngadu.common.view.BaseView;
import com.seliro.ngadu.modul.layout.view.UserView;
import com.seliro.ngadu.modul.service.api.SeliroApi;
import com.seliro.ngadu.modul.service.request.UserBody;
import com.seliro.ngadu.modul.service.wrapper.WrapperUser;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Halim on 10/26/17.
 */

public class UserPresenter extends BaseViewPresenter implements IUserPresenter {

    private UserView userView;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        userView = (UserView) baseView;
    }

    @Override
    public void loadSeliroData(SeliroApi seliroApi, UserBody userBody) {
        seliroApi.getUser(new Gson().toJson(userBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Subscriber<WrapperUser>() {

                            @Override
                            public void onCompleted() {
                                Log.d(getClass().getName(), "Kampret ");
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(getClass().getName(), "ERROR " + e);
                                userView.checkConnection();
                                userView.checkGpsLocation();
                            }

                            @Override
                            public void onNext(WrapperUser wrapperUser) {
                                Log.d(getClass().getName(), "Kampret " + wrapperUser.getUser());
                                userView.setSeliroView(wrapperUser.getUser());
                            }
                        });
    }
}
